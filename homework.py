from scapy.all import *
import sys
import time


DNS_SERVER_IP = '8.8.8.8'
DNS_SERVER_PORT = 53
CLIENT_LOCAL_PORT = 9095

PING_REQUEST_NUMBER = 3

EXPECTED_ARGUMENTS = 3
COMMAND_INDEX = 1
VALUE_INDEX = 2

IP_PARTS_NUMBER = 4
IP_MAX = 255
IP_MIN = 255
IP_SPLIT = '.'

NSLOOKUP_COMMAND = 'nslookup'
PING_COMMAND = 'ping'
TRACERT_COMMAND = 'tracert'


def is_valid_ip(ip_to_check):
    """
    The function get a string, and check if it ip address

    :param ip_to_check: regular string, type: str
    :return: True -> could be ip address, else -> False
    """
    if len(ip_to_check.split(IP_SPLIT)) != IP_PARTS_NUMBER:
        return False

    for num in ip_to_check.split(IP_SPLIT):

        if not num.isdigit():
            return False

        if int(num) > IP_MAX or int(num) < IP_MIN:
            return False

    return True


def ns_lookup_command(site_address):
    """
    The function get a website address and return the ip of the website

    :param site_address: the site address, type: str
    :return: ip_address -> type: str, error -> none
    """
    try:
        dns_request = IP(dst=DNS_SERVER_IP) / UDP(sport=CLIENT_LOCAL_PORT, dport=DNS_SERVER_PORT) / DNS(rd=1, qd=DNSQR(qname=site_address))
        dns_response = sr1(dns_request, verbose=0, timeout=5)

        if dns_response.rcode != 0:
            print(f"Error!")
            return

        return dns_response.an.rdata

    except Exception as e:
        print(f"Error!")


def ping_command(destination_ip):
    """
    The function get a ip and sent 3 simple requests, to check if he ok

    :param destination_ip: a dst ip, type: str
    :return: none
    """
    avg, start, end, lose = 0, 0, 0, 0

    for i in range(PING_REQUEST_NUMBER):
        start = time.time()

        try:
            full_msg = Ether() / IP(dst=destination_ip) / ICMP()
            answer = srp1(full_msg, verbose=0, timeout=3)
        except Exception:
            lose += 1
            print("Request timed out.")
            continue

        end = round(time.time() - start, 5)

        print(f"Reply from {destination_ip}, time: {end}ms")
        avg += end

    print(f"Total time: {avg}, average: {avg / PING_REQUEST_NUMBER}, lose: {lose}")


def trace_rt_command(ip_to_trace):
    """
    The function get ip, and trace the path to the ip

    :param ip_to_trace: the ip, type: str
    :return: none
    """
    t = 1

    print("    Time      IP")
    while True:
        start = time.time()
        try:
            full_msg = IP(dst=ip_to_trace, ttl=t) / ICMP()
            answer = sr1(full_msg, verbose=0, timeout=2)
            current_ip = answer[IP].src
        
        except Exception:
            print(f"{t} : *   Request timed out.")
            t += 1
            continue

        print(f"{t} : {round(time.time() - start, 3)}   {current_ip}")
        t += 1
        if current_ip == ip_to_trace:
            break


def main():
    if len(sys.argv) != EXPECTED_ARGUMENTS:
        print("Invalid number of arguments\nFormat: homework.py command data")
        sys.exit(1)

    command, data = sys.argv[COMMAND_INDEX].lower(), sys.argv[VALUE_INDEX]

    if command == PING_COMMAND:
        ping_command(data)

    elif command == NSLOOKUP_COMMAND:
        # maybe convert to ip
        if is_valid_ip(data):
            print(f"IP: {data}")
        else:
            print(f"IP: {ns_lookup_command(data)}")

    elif command == TRACERT_COMMAND:
        # maybe convert to ip
        if is_valid_ip(data):
            trace_rt_command(data)
        else:
            trace_rt_command(ns_lookup_command(data))

    else:
        print("Invalid command, try ping/ nslookup/ tracert")


if __name__ == '__main__':
    main()
